package examples.quickprogrammingtips.com.tablayout;

/**
 * Created by anton on 26-11-15.
 */
public class TwoStrings {
    private String one;
    private String two;
    public TwoStrings(String one, String two){
        this.setOne(one);
        this.setTwo(two);
    }

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }
}
